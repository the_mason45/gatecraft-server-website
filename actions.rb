def authorized?
  session[:authorized]
  session[:authorized_as]
end

def authorize!
  redirect '/login' unless authorized?
end

def logout!
  session[:authorized] = false
  session[:authorized_as] = nil
end
