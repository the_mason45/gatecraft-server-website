require 'rubygems'
require 'sinatra'
require 'data_mapper'
require 'dm-migrations'
require 'erb'
require 'digest/md5'
require './routes.rb'
require './actions.rb'

set :bind, '0.0.0.0'
enable :sessions

DataMapper.setup(:default, 'sqlite:project.db')

class Search
  include DataMapper::Resource
  property :id,         Serial
  property :name,			  String
  property :data,       Text
end

class User
  include DataMapper::Resource
  property :id,         Serial
  property :name,			  String
  property :password,   String
end

class Message
  include DataMapper::Resource
  property :id,         Serial
  property :sender,			String
  property :recipitent, String
  property :message,    Text
end

class TrainRoute
  include DataMapper::Resource
  property :id,         Serial
  property :destination,String
  property :start,      String
  property :user,       String
end

DataMapper.finalize

DataMapper.auto_upgrade!


get '/' do
  erb :home
end

get '/sam_Texture' do
  redirect 'http://www.dropbox.com/sh/m5a8w9iw3sz3v6m/AABGrAJQJTNFG0hPb2AguBH1a?dl=0'
end

get '/user/Sam' do
  authorize!
  erb :sam
end

get '/user/Ben' do
  authorize!
  erb :ben
end

post '/application/Messaging' do
  recipitent = params[:recipitent]
  message = params[:message]
  sender = session[:authorized_as]

  data = Message.create(
    :sender => sender,
    :recipitent => recipitent,
    :message => message
  )
  p data.inspect
  redirect '/user/' + session[:authorized_as]
end

get '/login' do
  if session[:authorized_as] != nil then
    redirect '/user/' + session[:authorized_as]
  else
    erb :login
  end
end

get '/book/train' do
  authorize!
  erb :book_train
end

post '/application/travel/train/booking' do
  destination = params[:destination]
  start = params[:start]
  user = session[:authorized_as]

  Route = TrainRoute.create(
    :destination => destination,
    :start => start,
    :user => user
  )

  redirect '/view/journeys'
end

get '/view/journeys' do
  authorize!
  @journeys = TrainRoute.all(:user => session[:authorized_as])
  erb :journeys
end

get '/logout' do
  logout!
  redirect "/"
end

get '/application/Messaging' do
  authorize!
  erb :messaging
end

post '/login' do
  name = params[:name]
  password = params[:password]
  p User.last(:password => 'Sam')
  @loginDetails = User.last(:name => name)
  @loginDetails.inspect
  if name == @loginDetails.name && password == @loginDetails.password then
    session[:authorized] = true
    session[:authorized_as] = name
    redirect '/user/' + name
  else
    session[:authorized] = false
    session[:authorized_as] = nil
    redirect '/'
  end
end

get '/downloads' do
  erb :downloads
end

get '/add/search' do
  authorize!
  erb :add_result
end

post '/add/search' do
  @name = params[:name]
  @data = params[:data]
  search = Search.create(
    :name => @name,
    :data => @data
  )
  p search
end

# HTTP functions below

get '/HTTP/login/:user/:password' do
  name = params[:user]
  password = params[:password]
  p User.last(:password => 'Sam')
  loginDetails = User.last(:name => name)
  if name == loginDetails.name && password == loginDetails.password then
    return true
  else
    return false
  end
end

get '/HTTP/view/trains/:user/:start' do
  user = params[:user]
  start = params[:start]
  journeys = TrainRoute.last(:user => user, :start => start)
  journeys.destination
end

get '/HTTP/control/trains/:user/:start' do
  user = params[:user]
  start = params[:start]
  journey = TrainRoute.last(:user => user, :start => start)
  switch = get_route(journey.start,journey.destination)
  switch.each do |n|
    n[1]
  end
end

get '/HTTP/control/trains/remove/:user/:start' do
  user = params[:user]
  start = params[:start]
  journey = TrainRoute.last(:user => user, :start => start)
  journey.destroy
end

get '/HTTP/search/:search' do
  query = params[:search]
  @result = Search.last(:name => query)
  @result.data
end

get '/HTTP/messages/:recipitentName/:senderName' do
  sender = params[:senderName]
  recipitent = params[:recipitentName]
  messages = Message.all(:sender => sender, :recipitent => recipitent)
  messages.each do |n|
    n.message
  end
end
