def get_route(start,destination)
  if start == "Paris_Central" && destination == "London_Central" then
    switch = paris_to_london
    return switch
  end
  if start == "London_Central" && destination == "Paris_Central" then
    switch = london_to_paris
    return switch
  end
end

def paris_to_london
  junction = [
    "0","g"
  ]
  return junction
end

def london_to_paris
  junction = [
    "0","m"
  ]
  return junction
end
